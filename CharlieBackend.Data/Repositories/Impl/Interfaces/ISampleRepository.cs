﻿using CharlieBackend.Core.Entities;

namespace CharlieBackend.Data.Repositories.Impl.Interfaces
{
    public interface ISampleRepository : IRepository<Sample>
    {
    }
}
